package com.definitionbuddy;

public class AppNoFactoryBad {
    public static void main(String[] args) {
        // initial code - dependencies inside

//        GeneralDictionaryBad d = new GeneralDictionaryBad();
//        List<String> definitions = d.getDefinitions("computer");
//        definitions.forEach(System.out::println);
//
//
//        // With injected dependencies - but:
//        // 1) We complicate the client code
//        // 2) We expose unnecessary details (we leak what should be perhaps encapsulated)
//
//        GeneralDictionaryBad d2 = new GeneralDictionaryBad(new WebServiceDefinitionSearchBad());
//        List<String> definitions2 = d2.getDefinitions("book");
//        definitions2.forEach(System.out::println);
//
//        // Dict with Local Backup Injected - use it if the WebService is unavailable
//        GeneralDictionaryBad d3 = new GeneralDictionaryBad(new WebServiceDefinitionSearchBad(new HttpHelperBad(), Language.SPANISH));
//        List<String> definitions3 = d3.getDefinitions("hola");
//        definitions3.forEach(System.out::println);
//
//
//        GeneralDictionaryBad d4 = GeneralDictionaryBad.english();
//        GeneralDictionaryBad d5 = GeneralDictionaryBad.spanish();
//
//
//        List<String> definitions4 = d4.getDefinitions("tea");
//        definitions4.forEach(System.out::println);
//
//        List<String> definitions5 = d5.getDefinitions("hola");
//        definitions5.forEach(System.out::println);
//
//        Dictionary d6 = SimpleDictionaryFactory.english();
//        d6.getDefinitions("keyboard").forEach(System.out::println);
//
//        Dictionary d7 = SimpleDictionaryFactory.espanish();
//        d7.getDefinitions("doctor").forEach(System.out::println);
//
//        DictionaryFactory dictionaryFactory = new GeneralDictionaryFactory();
//
//
//        dictionaryFactory.newDictionary(Language.ENGLISH)
//                .getDefinitions("coffe")
//                .forEach(System.out::println);


    }
}
