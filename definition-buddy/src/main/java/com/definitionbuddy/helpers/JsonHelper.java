package com.definitionbuddy.helpers;

import com.definitionbuddy.entities.api.DataResponse;
import com.definitionbuddy.entities.api.Response;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.NameTokenizers;
import org.modelmapper.jackson.JsonNodeValueReader;

import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;

import static java.util.Arrays.stream;
import static java.util.stream.Collectors.toList;

public class JsonHelper {

    ModelMapper mapper;
    /**
     * Accepts a Json String and extracts definitions from lines prefixed with "definition"
     * @param json properly formatted Json String
     * @return a list of definitions
     */
    public static List<String> extractDefinitions(String json) {

        Predicate<String> nonEmptyDefinition = line -> line.contains("definition") &&
                                               line.substring(line.lastIndexOf(":")+ 3).trim().length() > 3;

        Function<String, String> extractDefinition = line -> line.substring(line.lastIndexOf(":") + 3, line.length() - 2);

        List<String> defs =
                stream(json.split("\\n"))
                .filter(nonEmptyDefinition)
                .map(extractDefinition)
                .collect(toList());

        return defs.isEmpty() ? List.of("Nothing found") : defs;
    }

    public static List<String> extractDefinitionsByApi(String json) {
        ModelMapper modelMapper = new ModelMapper();
        //modelMapper.getConfiguration().addValueReader(new JsonNodeValueReader());
        modelMapper.getConfiguration().setSourceNameTokenizer(NameTokenizers.UNDERSCORE);

        //json = json.substring(1, json.length()-1);

        JsonNode responseNode = null;
        try {
            responseNode = new ObjectMapper().readTree(json);
            DataResponse response = modelMapper.map(json, DataResponse.class);

            System.out.println("TEST" + responseNode.asText());
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        return List.of("Nothing found");
    }
}
