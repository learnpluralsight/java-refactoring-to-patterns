package com.definitionbuddy.entities.api;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import java.util.List;

public class Response {

    private String word;

    //private List<Meaning> meanings;

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

//    public List<Meaning> getMeanings() {
//        return meanings;
//    }
//
//    public void setMeanings(List<Meaning> meanings) {
//        this.meanings = meanings;
//    }
}
