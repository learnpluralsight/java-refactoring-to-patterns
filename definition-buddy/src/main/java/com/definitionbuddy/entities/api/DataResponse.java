package com.definitionbuddy.entities.api;

import java.util.List;

public class DataResponse {

    List<Response> responseList;

    public List<Response> getResponseList() {
        return responseList;
    }

    public void setResponseList(List<Response> responseList) {
        this.responseList = responseList;
    }
}
