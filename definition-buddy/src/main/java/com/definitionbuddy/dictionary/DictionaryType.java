package com.definitionbuddy.dictionary;

public enum DictionaryType {

    GENERAL("general"),
    LEGAL("legal"),
    MEDICAL("medical");

    private String dictionaryType;

    DictionaryType(String dictionaryType){
        this.dictionaryType = dictionaryType;
    }

    @Override
    public String toString(){
        return dictionaryType;
    }
}
