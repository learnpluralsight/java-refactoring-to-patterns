package com.definitionbuddy;

import com.definitionbuddy.dictionary.GeneralDictionary;
import com.definitionbuddy.search.LocalBackupDefinitionSearch;
import com.definitionbuddy.search.WebServiceDefinitionSearch;
import org.junit.platform.commons.util.StringUtils;

import java.util.Formatter;
import java.util.List;

public class AppNoFactory {
    public static void main(String[] args) {

        String accountNumber = "84001488";

        if(accountNumber!=null){
			/*
            Format Cross FFFFFFF1SSS2
            Format TO NV MMSSSFFFFFFF12
            */
            final int LENGTH_FORMAT_CROSS_ACCOUNT = 12;
            if(accountNumber.length() < LENGTH_FORMAT_CROSS_ACCOUNT){
                try (Formatter accountCrossFormatted = new Formatter()) {
                    accountCrossFormatted.format("%012d", Integer.parseInt(accountNumber));
                    accountNumber = accountCrossFormatted.toString();
                } catch (Exception ex) {
                    //logger.error("Error in formatter Account Cross: {}", ex.toString());
                }
            }

            StringBuilder accountBuilder = new StringBuilder();

            //Pesos Currency - MM
            //TODO review currency hardcode in '00'
            accountBuilder.append("00");

            //Sucursal - SSS
            String sucursal = accountNumber.substring(8,11);

            accountBuilder.append(sucursal);

            //Folio - FFFFFFF
            String folio = accountNumber.substring(0, 7);
            accountBuilder.append(folio);

            //First Code Security - 1
            String firstCodeSecurity = accountNumber.substring(7, 8);
            accountBuilder.append(firstCodeSecurity);

            //Second Code Security - 2
            String secondCodeSecurity = accountNumber.substring(11, 12);
            accountBuilder.append(secondCodeSecurity);

            String accountFormatNV = accountBuilder.toString();


            //logger.info("Process formatting Account:{} to NV format:{}", accountNumber, accountFormatNV);
            //return accountFormatNV;
            System.out.println("Process formatting Account:"+ accountNumber +" to NV format: "+ accountFormatNV);;
        }

        // 00148000084008
        // 00148000084008
        // initial code - dependencies inside

        GeneralDictionary d = new GeneralDictionary();
        List<String> definitions = d.getDefinitions("computer");
        definitions.forEach(System.out::println);


        // With injected dependencies - but:
        // 1) We complicate the client code
        // 2) We expose unnecessary details (we leak what should be perhaps encapsulated)

        GeneralDictionary d2 = new GeneralDictionary(WebServiceDefinitionSearch.newInstance());
        List<String> definitions2 = d2.getDefinitions("book");
        definitions2.forEach(System.out::println);


        // Dict with Local Backup Injected - use it if the WebService is unavailable
        GeneralDictionary d3 = new GeneralDictionary(LocalBackupDefinitionSearch.newInstance());
        List<String> definitions3 = d3.getDefinitions("tea");
        definitions3.forEach(System.out::println);
    }
}
