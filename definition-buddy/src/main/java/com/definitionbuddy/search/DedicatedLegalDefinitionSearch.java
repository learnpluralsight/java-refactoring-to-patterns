package com.definitionbuddy.search;

import com.definitionbuddy.dictionary.Language;

import java.util.List;

public class DedicatedLegalDefinitionSearch implements DefinitionSearch{

    private Language language;

    public DedicatedLegalDefinitionSearch(){
        this.language = Language.ENGLISH;
    }

    public DedicatedLegalDefinitionSearch(Language language){
        this.language = language;
    }

    @Override
    public List<String> getDefinition(String word) {
        return null;
    }
}
